clc;
clear;
close all;

% create dummy matrices for testing
s = 2000;
X = rand(s);
Y = rand(s);
CorrMat = X*X';
DistanceWeights = abs(Y*Y');

disp("Running weightedCov mex function...");
tic;
Cpp_WeightedCorrCorrMat = weightedCov(CorrMat, DistanceWeights);
toc

disp("Running weightedCov MATLAB script...");
tic;
WeightedCorrCovMat = zeros(size(CorrMat));
for i = 1:size(CorrMat,2)
    DistWeight_i = DistanceWeights(:,i);
    CorMatCol_i = CorrMat(:,i);
    
    for j = i:size(CorrMat,2)
        DistWeight_j = DistanceWeights(:,j);
        DistWeight_ij = DistWeight_i .* DistWeight_j;
        DistWeight_ij_Sum = sum(DistWeight_ij);

        ThisWeightedMean_i = sum(CorMatCol_i .* DistWeight_ij) ./ ...
            DistWeight_ij_Sum;
        ThisWeightedMean_j = sum(CorrMat(:,j) .* DistWeight_ij) ./ ...
            DistWeight_ij_Sum;

        ThisWeightedCov = sum(DistWeight_ij .* ...
            (CorMatCol_i-ThisWeightedMean_i) .* ...
            (CorrMat(:,j)-ThisWeightedMean_j),1) ./ DistWeight_ij_Sum;

        WeightedCorrCovMat(i,j) = ThisWeightedCov;
        WeightedCorrCovMat(j,i) = ThisWeightedCov;
    end
end
toc

a = WeightedCorrCovMat;
b = Cpp_WeightedCorrCorrMat;
atol = 1e-8;
rtol = 1e-5;
all( abs(a(:)-b(:)) <= atol+rtol*abs(b(:)) )

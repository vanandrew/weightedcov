#################### MAKE SURE THESE VALUES ARE CORRECT! ####################
# define matlab dir
MDIR = /opt/MATLAB/R2021a
BLISDIR = ./blis
# if not set by env variables set it here
# MKLROOT = /opt/intel/mkl

# compiles mex files using g++ (MAKE SURE THIS IS RIGHT!)
CC = g++-9
#############################################################################

# compiler flags for g++
CCFLAGS = -fexceptions -fPIC -pthread -std=c++11 -O3 -march=native -fopenmp -g

# define which files to be included
CINCLUDE = -I$(MDIR)/extern/include

#Blis includes
BLISINC = -I$(BLISDIR)/include/skx

# Blis flags
BLISLIB = -Wl,-rpath,$(BLISDIR)/lib/skx -L$(BLISDIR)/lib/skx -lblis

# MKL includes
MKLINC = -I$(MKLROOT)/include

# MKL flags
MKLLIB = -Wl,-rpath,$(MKLROOT)/lib/intel64 -L${MKLROOT}/lib/intel64 -lmkl_rt -lmkl_intel_lp64 -lmkl_intel_thread \
-lmkl_core -liomp5 -lpthread -lm -ldl

# define MATLAB flags
MFLAGS = -DMATLAB_DEFAULT_RELEASE=R2017b -DUSE_MEX_CMD -D_GNU_SOURCE -DMATLAB_MEX_FILE

# define more MATLAB flags
MMFLAGS = -Wl,--as-needed -Wl,-rpath-link,$(MDIR)/bin/glnxa64 -Wl,-rpath-link,$(MDIR)/extern/bin/glnxa64 \
-L$(MDIR)/bin/glnxa64 -L$(MDIR)/extern/bin/glnxa64 -lstdc++ -lMatlabDataArray -lmx -lmex -lm -lmat 

# define link flags
LFLAGS = -shared -Wl,--no-undefined -Wl,--version-script,$(MDIR)/extern/lib/glnxa64/c_exportsmexfileversion.map

all : weightedCov.mexa64

weightedCov.o:weightedCov.cpp
	$(CC) -c $(MKLINC) $(MKLLIB) $(BLISINC) $(BLISLIB) $(MFLAGS) $(CINCLUDE) $(CCFLAGS) -Ic++ $< -o $@

cpp_mexapi_version.o:$(MDIR)/extern/version/cpp_mexapi_version.cpp
	$(CC) -c $(MFLAGS) $(CINCLUDE) $(CCFLAGS) -Ic++ $< -o $@

weightedCov.mexa64:weightedCov.o cpp_mexapi_version.o
	$(CC) $(MKLINC) $(MKLLIB) $(BLISINC) $(BLISLIB) $(CCFLAGS) $(LFLAGS) $< $(MMFLAGS) -o $@

# clean up
clean:
	rm -f *.o weightedCov.mexa64

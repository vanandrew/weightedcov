#include "blis.h"
#include "mkl_vml.h"

#include "mex.hpp"
#include "mexAdapter.hpp"

class MexFunction : public matlab::mex::Function {
public:
    using ArgumentList = matlab::mex::ArgumentList;
    using ArrayDimensions = matlab::data::ArrayDimensions;
    template <class T> using TypedArray = matlab::data::TypedArray<T>;
    template <class T> using buffer_ptr_t = matlab::data::buffer_ptr_t<T>;
    matlab::data::ArrayFactory factory;
    
    // for debugging
    void print_vector(const char* str, obj_t &vec) {
        bli_printv((char*)str, &vec, (char*)"%.5f", (char*)"");
    }
    // for debugging
    void print_matrix(const char* str, obj_t &mat) {
        bli_printm((char*)str, &mat, (char*)"%.5f", (char*)"");
    }

    // mex function
    void operator()(ArgumentList outputs, ArgumentList inputs) {
        // get the inputs
        TypedArray<double> X = std::move(inputs[0]);
        TypedArray<double> W = std::move(inputs[1]);

        // get size of X, W
        ArrayDimensions dims = X.getDimensions();
        size_t numRows = dims[0];
        size_t numCols = dims[1];

        // get ptrs to buffers for X and W
        buffer_ptr_t<double> X_buffer = X.release();
        buffer_ptr_t<double> W_buffer = W.release();

        // create output buffer the same size as X
        buffer_ptr_t<double> O_buffer(factory.createBuffer<double>(X.getNumberOfElements()));        

        // create blis objs on input/output matrices
        // DO NOT CALL DELETE OR FREE THE BUFFERS ON THESE OBJECTS
        // OR YOU WILL PROBABLY GET A SEGFAULT
        // MATLAB has a unique_ptr to the buffer and will clean them up when
        // the ptr goes out of scope
        obj_t X_blis, W_blis, O_blis;
        bli_obj_create_with_attached_buffer(BLIS_DOUBLE, numRows, numCols, X_buffer.get(), 1, numRows, &X_blis);
        bli_obj_create_with_attached_buffer(BLIS_DOUBLE, numRows, numCols, W_buffer.get(), 1, numRows, &W_blis);
        bli_obj_create_with_attached_buffer(BLIS_DOUBLE, numRows, numCols, O_buffer.get(), 1, numRows, &O_blis);

        // set properties for X and W, they are both symmetric
        // probably not necessary since we never operate on these matrices
        // directly
        bli_obj_set_struc(BLIS_SYMMETRIC, &X_blis);
        bli_obj_set_struc(BLIS_SYMMETRIC, &W_blis);

        // create blis objects for row/col
        // Note that these are all created as column vectors
        // and NOT initialized.
        // We will assignt them values in the nested for loop
        obj_t Xcol_blis, Wcol_blis, Xrow_blis, Wrow_blis;
        bli_obj_create_without_buffer(BLIS_DOUBLE, numRows, 1, &Xcol_blis);
        bli_obj_create_without_buffer(BLIS_DOUBLE, numRows, 1, &Wcol_blis);
        bli_obj_create_without_buffer(BLIS_DOUBLE, numRows, 1, &Xrow_blis);
        bli_obj_create_without_buffer(BLIS_DOUBLE, numRows, 1, &Wrow_blis);
        // initialize pointers for cols/rows
        double *Xcol_ptr, *Xrow_ptr, *Wcol_ptr, *Wrow_ptr;

        // initialize some more pointers for stuff
        double *WeightCov_ptr, *WrowWcol_ptr, *buffer0_ptr, *buffer1_ptr;

        // loop for each col of X and W
        #pragma omp parallel for \
        default(none) \
        shared(X_buffer, W_buffer, O_buffer, numCols, numRows, BLIS_MINUS_ONE) \
        firstprivate(Xcol_blis, Wcol_blis, Xrow_blis, Wrow_blis) \
        private(Xcol_ptr, Wcol_ptr, Xrow_ptr, Wrow_ptr, \
        WeightCov_ptr, WrowWcol_ptr, buffer0_ptr, buffer1_ptr) \
        schedule(dynamic) num_threads(8)
        for (size_t i = 0; i < numCols; i++) {
            // get the X and W cols
            bli_obj_attach_buffer(X_buffer.get() + i*numRows, 1, numRows, 0, &Xcol_blis);
            bli_obj_attach_buffer(W_buffer.get() + i*numRows, 1, numRows, 0, &Wcol_blis);
            // get pointers to col buffers
            Xcol_ptr = static_cast<double*>(bli_obj_buffer(&Xcol_blis));
            Wcol_ptr = static_cast<double*>(bli_obj_buffer(&Wcol_blis));

            // loop over rows
            for (size_t j = i; j < numRows; j++) {
                // get the X and W rows
                bli_obj_attach_buffer(X_buffer.get() + j*numRows, 1, numRows, 0, &Xrow_blis);
                bli_obj_attach_buffer(W_buffer.get() + j*numRows, 1, numRows, 0, &Wrow_blis);
                // get pointers to row buffers
                Xrow_ptr = static_cast<double*>(bli_obj_buffer(&Xrow_blis));
                Wrow_ptr = static_cast<double*>(bli_obj_buffer(&Wrow_blis));

                // create objs for results
                obj_t Wsum, WMeanCol, WMeanRow, WeightCov, WrowWcol;
                bli_obj_create_1x1(BLIS_DOUBLE, &Wsum);
                bli_obj_create_1x1(BLIS_DOUBLE, &WMeanCol);
                bli_obj_create_1x1(BLIS_DOUBLE, &WMeanRow);
                bli_obj_create_1x1(BLIS_DOUBLE, &WeightCov);
                bli_obj_create(BLIS_DOUBLE, numRows, 1, 1, numRows, &WrowWcol);
                // make pointers to some objs
                WeightCov_ptr = static_cast<double*>(bli_obj_buffer(&WeightCov));
                WrowWcol_ptr = static_cast<double*>(bli_obj_buffer(&WrowWcol));

                // create an identity object for subtracting scalars
                obj_t idn_sub;
                bli_obj_create(BLIS_DOUBLE, numRows, 1, 1, numRows, &idn_sub);
                bli_setv(&BLIS_MINUS_ONE, &idn_sub);

                // create a temp buffers
                obj_t buffer0, buffer1;
                bli_obj_create(BLIS_DOUBLE, numRows, 1, 1, numRows, &buffer0);
                bli_obj_create(BLIS_DOUBLE, numRows, 1, 1, numRows, &buffer1);
                // make pointers to buffers
                buffer0_ptr = static_cast<double*>(bli_obj_buffer(&buffer0));
                buffer1_ptr = static_cast<double*>(bli_obj_buffer(&buffer1));

                // compute dot product
                // compute Wcol * Wrow
                vdMul(numRows, Wcol_ptr, Wrow_ptr, WrowWcol_ptr);
                // compute sum(Wcol * Wrow)
                bli_asumv(&WrowWcol, &Wsum);
                // invert Wsum
                bli_invertv(&Wsum);

                // compute weighted means
                // do cols i
                // compute dot product (sum(Xcol * (Wrow * Wcol)))
                bli_dotv(&Xcol_blis, &WrowWcol, &WMeanCol);
                // scale WMeanCol
                bli_scalv(&Wsum, &WMeanCol);
                // do rows j
                // compute dot product (sum(Xrow * (Wrow * Wcol)))
                bli_dotv(&Xrow_blis, &WrowWcol, &WMeanRow);
                 // scale WMeanRow
                bli_scalv(&Wsum, &WMeanRow);

                // compute weighted covariance
                // copy Xcol, Xrow so we don't override the original values
                bli_copyv(&Xcol_blis, &buffer0);
                bli_copyv(&Xrow_blis, &buffer1);
                // compute Xcol - WMeanCol
                bli_axpyv(&WMeanCol, &idn_sub, &buffer0);
                // buffer0 now stores Xcol - WMeanCol
                // compute Xrow - WMeanRow
                bli_axpyv(&WMeanRow, &idn_sub, &buffer1);
                // buffer1 now stores Xrow - WMeanRow
                // compute  (Xcol - WMeanCol) * (Xrow - WMeanRow)
                vdMul(numRows, buffer0_ptr, buffer1_ptr, buffer0_ptr);
                // buffer0 now stores (Xcol - WMeanCol) * (Xrow - WMeanRow)
                // compute sum((Wrow * Wcol) * (Xcol - WMeanCol) * (Xrow - WMeanRow))
                bli_dotv(&WrowWcol, &buffer0, &WeightCov);
                // scale by Wsum
                bli_scalv(&Wsum, &WeightCov);

                // store the result in (i,j)
                O_buffer[i*numRows + j] = *WeightCov_ptr;
                
                // store the result in (j,i)
                O_buffer[j*numRows + i] = *WeightCov_ptr;

                // clean up created objects
                bli_obj_free(&Wsum);
                bli_obj_free(&WMeanCol);
                bli_obj_free(&WMeanRow);
                bli_obj_free(&WeightCov);
                bli_obj_free(&WrowWcol);
                bli_obj_free(&idn_sub);
                bli_obj_free(&buffer0);
                bli_obj_free(&buffer1);
            }
        }

        // return output
        outputs[0] = std::move(factory.createArrayFromBuffer(X.getDimensions(), std::move(O_buffer)));
    }
};
